package com.example.crud.Controladores;

import com.example.crud.Modelos.Persona;
import com.example.crud.Repositorios.Repositorio;
import org.aspectj.weaver.patterns.PerObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost")
public class Controlador{
    
    @Autowired
    private Repositorio repositorio;

    @GetMapping(value = "/")
    public String hola(){
        return "Hola!!!";
    }

    @GetMapping(value = "/personas")
    public List<Persona> getPersonas(){
        return repositorio.findAll();
    }

    @PostMapping(value = "/guardar")
    public String guardarPersona(@RequestBody Persona persona){
        repositorio.save(persona);
        return "Se guardo correctamente";
    }

    @PutMapping(value = "/actualizar/{id}")
    public String actualizarPersona(@PathVariable long id, @RequestBody Persona persona){
        Persona aPersona = repositorio.getById(id);
        aPersona.setNombre(persona.getNombre());
        aPersona.setApellido(persona.getApellido());
        repositorio.save(aPersona);
        return "Se actualizo correctamente";
    }

    @DeleteMapping(value = "eliminar/{id}")
    public String eliminarPersona(@PathVariable long id){
        Persona ePersona = repositorio.getById(id);
        repositorio.delete(ePersona);
        return "Se elimino correctamente";
    }
}
