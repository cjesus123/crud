package com.example.crud.Repositorios;


import com.example.crud.Modelos.Persona;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Repositorio extends JpaRepository<Persona,Long> {
}
